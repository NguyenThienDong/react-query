export const vi_component = {
  'component.search.request': 'Yêu cầu',
  'component.search.reset': 'Đặt lại',
  'component.search.name': 'Tên',
  'component.search.sex': 'Giới tính',
  'component.search.male': 'Nam',
  'component.search.woman': 'Nữ',
};
