export const vi_permissionRole = {
  'app.permission.role.name': 'Tên vai trò',
  'app.permission.role.code': 'Mã vai trò',
  'app.permission.role.status': 'Trạng thái',
  'app.permission.role.status.all': 'Tất cả',
  'app.permission.role.status.enabled': 'Đã bật',
  'app.permission.role.status.disabled': 'Đã tắt',
  'app.permission.role.nameRequired': 'Vui lòng nhập tên vai trò',
  'app.permission.role.codeRequired': 'Vui lòng nhập mã vai trò',
  'app.permission.role.statusRequired': 'Vui lòng chọn trạng thái đã bật',
};
