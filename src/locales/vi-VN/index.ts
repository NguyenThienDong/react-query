import { vi_account } from './account';
import { vi_component } from './component';
import { vi_dashboard } from './dashboard';
import { vi_globalTips } from './global/tips';
import { vi_guide } from './guide';
import { vi_notice } from './notice';
import { vi_permissionRole } from './permission/role';
import { vi_avatorDropMenu } from './user/avatorDropMenu';
import { vi_tagsViewDropMenu } from './user/tagsViewDropMenu';
import { vi_title } from './user/title';

const vi_VN = {
  ...vi_account,
  ...vi_avatorDropMenu,
  ...vi_tagsViewDropMenu,
  ...vi_title,
  ...vi_globalTips,
  ...vi_permissionRole,
  ...vi_dashboard,
  ...vi_guide,
  ...vi_notice,
  ...vi_component,
};

export default vi_VN;
