export const vi_title = {
  'title.login': 'Đăng nhập',
  'title.dashboard': 'Bảng điều khiển',
  'title.documentation': 'Tài liệu',
  'title.guide': 'Hướng dẫn',
  'title.permission.route': 'Quyền Lộ trình',
  'title.permission.button': 'Quyền của Nút',
  'title.permission.config': 'Cấu hình quyền',
  'title.account': 'Tài khoản',
  'title.notFount': '404',
};
