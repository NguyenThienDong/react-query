import type { ColumnsType, TableProps } from 'antd/es/table';

import './index.less';

import { Table } from 'antd';
import React from 'react';

type TableBaseProps = {
  columns: ColumnsType[] | any[];
  dataSource: any[];
  pagination?: any;
  iconEmpty?: React.ReactNode;
  textEmpty?: string;
  rowSelection?: any;
  scroll?: object;
  rowKey?: string;
  loading?: boolean;
  onChange?: TableProps<any>['onChange'];
};

const TableBase: React.FC<TableBaseProps> = ({
  columns = [],
  dataSource,
  pagination = false,
  textEmpty,
  rowSelection,
  scroll,
  rowKey = 'id',
  loading = false,
  onChange,
}) => {
  return (
    <div className="mb-table">
      <Table
        columns={columns}
        dataSource={dataSource}
        pagination={pagination}
        onChange={onChange}
        loading={loading}
        locale={{
          emptyText: (
            <div className="table-empty">
              <span>{textEmpty || 'Không có dữ liệu'}</span>
            </div>
          ),
        }}
        rowSelection={rowSelection}
        scroll={scroll}
        rowKey={rowKey}
      />
    </div>
  );
};

export default React.memo(TableBase);
