import type { PaginationProps } from 'antd';

import { Pagination } from 'antd';
import React from 'react';

type PaginationBaseProps = {
  onPaginationChange: any;
  defaultCurrent?: number;
  current: number;
  totalRecords: number;
};

const PaginationBase: React.FC<PaginationBaseProps> = ({
  onPaginationChange,
  defaultCurrent,
  current,
  totalRecords,
}) => {
  const onShowSizeChange: PaginationProps['onShowSizeChange'] = (current, pageSize) => {
    onPaginationChange(current, pageSize);
  };

  return (
    <Pagination
      showSizeChanger
      onChange={onShowSizeChange}
      defaultCurrent={defaultCurrent}
      total={totalRecords}
      current={current}
      showTotal={(total, range) => `${range[0]}-${range[1]} of ${total} items`}
    />
  );
};

export default PaginationBase;
