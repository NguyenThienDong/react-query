import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { Col, Form, Input } from 'antd';
import React from 'react';

type PasswordBaseIProps = {
  name: string;
  label: string;
  placeholder?: string;
  required?: boolean;
  min?: number;
  max?: number;
  readOnly?: boolean;
  autoFocus?: boolean;
  prefix?: React.ReactNode;
  addonBefore?: React.ReactNode | string;
  addonAfter?: React.ReactNode | string;
  onChange?: any;
  span?: number;
  format?: string;
  status?: 'warning' | 'error';
  pattern?: RegExp;
  onClick?: any;
  type?: any;
  maxLength?: number;
  rules?: any[];
  className?: string;
  hasFeedback?: boolean;
  validateStatus?: '' | 'warning' | 'error' | 'success' | 'validating';
  help?: React.ReactNode;
};

const PasswordBase: React.FC<PasswordBaseIProps> = ({
  label,
  name,
  placeholder = 'Mật khẩu',
  required = false,
  min,
  max,
  readOnly,
  autoFocus = false,
  onChange,
  addonBefore,
  addonAfter,
  status,
  span = 24,
  pattern,
  maxLength,
  rules = [],
  className,
  hasFeedback = false,
  validateStatus = '',
  help,
}) => {
  const handleChange = (e: any) => {
    if (onChange) {
      onChange(name, e);
    }
  };

  const showIcon = (visible: boolean) => {
    if (visible) {
      return <EyeTwoTone />;
    } else {
      return <EyeInvisibleOutlined />;
    }
  };

  return (
    <Col span={span}>
      <Form.Item
        name={name}
        label={label}
        hasFeedback={hasFeedback}
        validateStatus={validateStatus}
        help={help}
        rules={[
          ...rules,
          {
            required,
            message: `${label} không được để trống`,
          },
          {
            min,
            message: `${label} chứa ít nhất ${min} ký tự`,
          },
          {
            max,
            message: `${label} chứa không quá ${max} ký tự`,
          },
          {
            pattern,
            message: `${label} không hợp lệ`,
          },
        ]}
      >
        <Input.Password
          onChange={handleChange}
          readOnly={readOnly}
          placeholder={placeholder}
          addonBefore={addonBefore}
          addonAfter={addonAfter}
          autoFocus={autoFocus}
          status={status}
          maxLength={maxLength}
          className={className}
          iconRender={visible => showIcon(visible)}
        />
      </Form.Item>
    </Col>
  );
};

export default React.memo(PasswordBase);
