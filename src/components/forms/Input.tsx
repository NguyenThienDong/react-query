import { Form, Input } from 'antd';
import React from 'react';

type InputBaseIProps = {
  name: string;
  label: string;
  placeholder?: string;
  required?: boolean;
  min?: number;
  max?: number;
  readOnly?: boolean;
  typeInput?: 'TextArea' | 'Password';
  autoFocus?: boolean;
  prefix?: React.ReactNode;
  addonBefore?: React.ReactNode | string;
  addonAfter?: React.ReactNode | string;
  onChange?: any;
  span?: number;
  format?: string;
  status?: 'warning' | 'error';
  pattern?: RegExp;
  onClick?: any;
  type?: any;
  maxLength?: number;
  rules?: any[];
  className?: string;
  hasFeedback?: boolean;
  validateStatus?: '' | 'warning' | 'error' | 'success' | 'validating';
  help?: React.ReactNode;
};

const InputBase: React.FC<InputBaseIProps> = ({
  label,
  name,
  placeholder = 'Nhập dữ liệu',
  required = false,
  type = 'text',
  min,
  max,
  readOnly,
  autoFocus = false,
  onChange,
  addonBefore,
  addonAfter,
  status,
  prefix,
  pattern,
  maxLength,
  rules = [],
  className,
  hasFeedback = false,
  validateStatus = '',
  help,
}) => {
  const handleChange = (e: any) => {
    if (onChange) {
      onChange(name, e);
    }
  };

  return (
    <Form.Item
      name={name}
      label={label}
      hasFeedback={hasFeedback}
      validateStatus={validateStatus}
      help={help}
      rules={[
        ...rules,
        {
          required,
          message: `${label} không được để trống`,
        },
        {
          type,
          message: `Không đúng định dạng ${type}`,
        },
        {
          min,
          message: `${label} chứa ít nhất ${min} ký tự`,
        },
        {
          max,
          message: `${label} chứa không quá ${max} ký tự`,
        },
        {
          pattern,
          message: `${label} không hợp lệ`,
        },
      ]}
    >
      <Input
        type={type}
        onChange={handleChange}
        readOnly={readOnly}
        placeholder={placeholder}
        autoFocus={autoFocus}
        addonBefore={addonBefore}
        addonAfter={addonAfter}
        maxLength={maxLength}
        status={status}
        prefix={prefix}
        className={className}
      />
    </Form.Item>
  );
};

export default React.memo(InputBase);
