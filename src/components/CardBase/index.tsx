import { Card } from 'antd';

type CardBaseProps = {
  title?: React.ReactNode;
  bordered?: boolean;
  className?: string;
  content?: React.ReactNode;
};

const CardBase: React.FC<CardBaseProps> = ({ title, bordered = false, className, children }) => {
  return (
    <Card title={title} bordered={bordered} className={`card ${className}`}>
      {children}
    </Card>
  );
};

export default CardBase;
