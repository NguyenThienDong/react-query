/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_APP_ENV: string;
  readonly VITE_BASE_URL: string;
  readonly VITE_PORT: number;
  readonly VITE_KEY_CLOCK_URL: string;
  readonly VITE_TIME_NO_ACTION: number;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
