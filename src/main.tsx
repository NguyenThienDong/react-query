import './mock';

import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import ReactDOM from 'react-dom';

import App from './App';
import { history, HistoryRouter } from './routes/history';
import { GlobalStateProvider } from './utils/useGlobalState';

const queryClient = new QueryClient({});

ReactDOM.render(
  <QueryClientProvider client={queryClient}>
    <GlobalStateProvider>
      <HistoryRouter history={history}>
        <App />
      </HistoryRouter>
    </GlobalStateProvider>
    <ReactQueryDevtools initialIsOpen={false} />
  </QueryClientProvider>,
  document.getElementById('root'),
);
