import { apiRoutes } from '@/utils/apiRoutes';
import { useCreate, usePostList } from '@/utils/reactQuery';

export const useGetTodoList = (payload: any) => usePostList<any>(`${apiRoutes.getTodoList}`, payload);
export const useAddTodo = () => useCreate<any>(`${apiRoutes.getTodoList}`);
