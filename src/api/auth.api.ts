import { apiRoutes } from '@/utils/apiRoutes';

import api from '../utils/api';

export const apiLogin = (username: string, password: string) =>
  api.post<any>(`${apiRoutes.login}`, {
    msisdn: username,
    password,
    deviceId: '123344',
    os: 'IOS',
    requestId: '123123123',
    currentTime: '123123',
  });
