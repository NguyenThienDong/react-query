import type { PaginationProps } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import type { FC } from 'react';

import './index.less';

import { useQueryClient } from '@tanstack/react-query';
import { Button, Card, Col, Form, Row } from 'antd';
import { useState } from 'react';
import { AiOutlineEye } from 'react-icons/ai';
import { useNavigate } from 'react-router-dom';

import CardBase from '@/components/CardBase';
import { InputBase, SelectBase } from '@/components/forms';
import PaginationBase from '@/components/Paginator';
import TableBase from '@/components/TableBase';
import { apiRoutes } from '@/utils/apiRoutes';
import { useDelete, useDownload, usePostList } from '@/utils/reactQuery';

const DashBoardPage: FC = () => {
  const queryClient = useQueryClient();
  const navigate = useNavigate();
  const deleteTodo = useDelete(apiRoutes.deleteTodo);
  const exportTemplate = useDownload(apiRoutes.exportTemplateCar);
  const [form] = Form.useForm();
  const [filters, setFilters] = useState({
    pageSize: 10,
    pageIndex: 1,
    carType: '',
    name: '',
    status: '',
    requestId: 'afa7ee58-20fe-4a0b-8dc1-ad703d199dc5',
    currentTime: '2023-08-06T20:28:34.124Z',
  });
  const { data: todosResp, error, isFetching } = usePostList<any>(apiRoutes.getTodoList, filters);

  const handleDelete = (id: string) => {
    const body: any = {
      id,
      requestId: 'afa7ee58-20fe-4a0b-8dc1-ad703d199dc5',
      currentTime: '2023-08-06T20:28:34.124Z',
    };

    deleteTodo.mutate(body, {
      onSuccess: () => {
        queryClient.invalidateQueries([apiRoutes.getTodoList, filters]);
      },
    });
  };

  const handleExportTemplate = () => exportTemplate({});

  const columns: ColumnsType<any> = [
    {
      key: 'no',
      title: 'No',
      fixed: 'left',
      width: 20,
      render: (_: string, record: any, index: number) => index + 1,
    },
    {
      key: 'name',
      dataIndex: 'name',
      title: 'Name',
      width: 135,
      render: (_: string, record: any) => record.name,
    },
    {
      key: 'carType',
      dataIndex: 'carType',
      title: 'Type',
      width: 135,
      render: (_: string, record: any) => record.carType,
    },
    {
      key: 'licensePlate',
      title: 'License Plate',
      width: 185,
      render: (record: any) => record.licensePlate,
    },
    {
      key: 'action',
      title: 'THAO TÁC',
      width: 120,
      fixed: 'right',
      align: 'right',
      render: (record: any) => (
        <div className="table-btn-action flex-row-end">
          <Button
            icon={<AiOutlineEye />}
            type="link"
            size="small"
            onClick={() => {
              navigate(`/todos/${record.id}/edit`);
            }}
          />
          <Button
            icon={<AiOutlineEye />}
            type="link"
            size="small"
            onClick={() => {
              handleDelete(record.id);
            }}
          />
        </div>
      ),
    },
  ];

  const handleFilterTable = (formValue: any) => {
    setFilters({
      ...filters,
      carType: formValue.carType || '',
      status: formValue.status || '',
      name: formValue.name || '',
    });
  };

  const handlePagination: PaginationProps['onShowSizeChange'] = (current, pageSize) => {
    if (current !== filters.pageIndex) {
      setFilters({
        ...filters,
        pageIndex: current,
        pageSize,
      });
    } else {
      if (pageSize !== filters.pageSize) {
        setFilters({
          ...filters,
          pageIndex: 1,
          pageSize,
        });
      }
    }
  };

  return (
    <div className="page-content">
      <Card title={'Todo management'}>
        <Button onClick={() => navigate('add')}>Add Item</Button>
        <Form form={form} onFinish={handleFilterTable}>
          <Row justify="space-between" gutter={{ xs: 0, md: 20 }}>
            <Col xs={24} md={8}>
              <InputBase label="Name" name="name" />
            </Col>
            <Col xs={24} md={8}>
              <SelectBase label="Type" name="carType" options={[]} />
            </Col>
            <Col xs={24} md={8}>
              <SelectBase label="Status" name="status" options={[]} />
            </Col>
          </Row>
          <div className="flex-end">
            <Button htmlType="submit" type="primary">
              Search
            </Button>
          </div>
        </Form>
        <Row gutter={10} justify="end">
          {/* <Button onClick={handleImport}>Import</Button> */}
          <Button onClick={handleExportTemplate}>Export Template</Button>
          {/* <Button onClick={handleExportExcel}>Export Excel</Button> */}
        </Row>
        <div className="table-mbu" style={{ zIndex: 10 }}>
          <TableBase
            columns={columns}
            dataSource={!error ? todosResp?.data?.result.list : []}
            loading={!!isFetching}
            scroll={{ x: 1200, y: `calc(100vh - 300px)` }}
            rowKey="id"
          />
        </div>
        <div>
          <PaginationBase
            defaultCurrent={filters.pageIndex}
            current={filters.pageIndex}
            totalRecords={todosResp?.data?.result?.totalElements}
            onPaginationChange={handlePagination}
          />
        </div>
      </Card>
    </div>
  );
};

export default DashBoardPage;
