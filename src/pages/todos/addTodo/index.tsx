import { Button, Col, Form, Row } from 'antd';
import { useNavigate } from 'react-router-dom';

import { InputBase, SelectBase } from '@/components/forms';
import { apiRoutes } from '@/utils/apiRoutes';
import { useCreate } from '@/utils/reactQuery';

const carTypes = [
  {
    label: 'FOUR_SEATS',
    value: 'FOUR_SEATS',
  },
  {
    label: 'FIVE_SEATS',
    value: 'FIVE_SEATS',
  },
  {
    label: 'SEVEN_SEATS',
    value: 'SEVEN_SEATS',
  },
  {
    label: 'SIXTEEN_SEATS',
    value: 'SIXTEEN_SEATS',
  },
];

const AddTodo = () => {
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const { mutate, isLoading } = useCreate(`${apiRoutes.addTodo}`);

  const handleCreate = (formValues: any) => {
    console.log('formValues: ', formValues);
    mutate(formValues, {
      onSuccess: () => {
        navigate('/todos');
      },
    });
  };

  return (
    <Form form={form} layout="vertical" onFinish={handleCreate}>
      <Row justify="space-between" gutter={10}>
        <Col md={6}>
          <InputBase label="Name" name="name" required />
        </Col>
        <Col md={6}>
          <SelectBase label="Type" name="carType" options={carTypes} required />
        </Col>
        <Col md={6}>
          <InputBase label="License" name="licensePlate" required />
        </Col>
        <Col md={6}>
          <InputBase label="Model" name="model" required />
        </Col>
      </Row>
      <div>
        <Button htmlType="submit" loading={isLoading}>
          Search
        </Button>
      </div>
    </Form>
  );
};

export default AddTodo;
