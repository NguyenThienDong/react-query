import type { LoginParams } from '@/interface/user/login';

import './index.less';

import { Button, Form, notification } from 'antd';
import Cookies from 'js-cookie';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { apiLogin } from '@/api/auth.api';
import InputBase from '@/components/forms/Input';
import Password from '@/components/forms/Password';
import { useGlobalState } from '@/utils/useGlobalState';

const initialValues: LoginParams = {
  username: '',
  password: '',
};

const LoginForm = () => {
  const [api, contextHolder] = notification.useNotification();
  const navigate = useNavigate();
  const { locale } = useGlobalState();
  const [btnLoading, setBtnLoading] = useState(false);

  useEffect(() => {
    if (Cookies.get('token')) {
      navigate('/');
    }
  }, []);

  const onFinished = async (form: LoginParams) => {
    console.log(form);
    setBtnLoading(true);
    const { username, password } = form;

    try {
      const res = await apiLogin(username, password);

      if (res.data.result.accessTokenResponse) {
        const { access_token, expires_in, refresh_token, refresh_expires_in } = res.data.result.accessTokenResponse;
        const { avatar, email, name, phoneNumber, unit } = res.data.result.accountInfo;
        const userInfo = {
          avatar: avatar.url,
          email,
          name,
          phoneNumber,
          unit,
        };

        Cookies.set('token', access_token, { expires: expires_in });
        Cookies.set('refresh_token', refresh_token, { expires: refresh_expires_in });
        Cookies.set('user', JSON.stringify(userInfo));
        navigate(`/${locale}`);
      }
    } catch (error) {
      console.log('errro: ', error);
      api.error({
        message: 'Lỗi đăng nhập',
        placement: 'topRight',
        duration: 1,
        description: 'Thông tin đăng nhập không chính xác',
      });
    } finally {
      setBtnLoading(false);
    }
  };

  return (
    <div className="login-page">
      {contextHolder}
      <Form<LoginParams>
        onFinish={onFinished}
        className="login-page-form"
        initialValues={initialValues}
        layout="vertical"
      >
        <div className="login-page-form-title">
          <h2 className="mb-primary">Đăng nhập</h2>
          <span>Vui lòng nhập thông tin để tiến hành đăng nhập</span>
        </div>
        <InputBase
          name="username"
          required
          placeholder="Nhập tên người dùng..."
          autoFocus
          label="Tên người dùng"
          maxLength={100}
          className="input-login"
        />
        <Password
          label="Mật khẩu"
          required
          name="password"
          placeholder="Nhập mật khẩu..."
          maxLength={100}
          className="input-login"
        />
        <Form.Item>
          <Button htmlType="submit" type="primary" className="login-page-form_button" loading={btnLoading}>
            Đăng nhập
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default LoginForm;
