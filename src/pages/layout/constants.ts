import type { MenuList } from '@/interface/layout/menu.interface';

export const menuList: MenuList = [
  {
    code: 'dashboard',
    label: {
      vi: 'Thống kê',
      en: 'Dashboard',
    },
    icon: 'dashboard',
    path: 'dashboard',
  },
  {
    code: 'todos',
    label: {
      vi: 'Quản lý todos',
      en: 'Todo management',
    },
    icon: 'documentation',
    path: 'todos',
  },

  {
    code: 'permission',
    label: {
      vi: 'Truy cập',
      en: 'Permission',
    },
    icon: 'permission',
    path: '/permission',
    children: [
      {
        code: 'routePermission',
        label: {
          vi: 'Quyền truy cập',
          en: 'Route Permission',
        },
        path: '/permission/route',
      },
      {
        code: 'notFound',
        label: {
          vi: '404',
          en: '404',
        },
        path: '/permission/404',
      },
    ],
  },
  {
    code: 'component',
    label: {
      vi: 'Component',
      en: 'Component',
    },
    icon: 'permission',
    path: '/component',
    children: [
      {
        code: 'componentForm',
        label: {
          vi: 'Form',
          en: 'Form',
        },
        path: 'component/form',
      },
      {
        code: 'componentTable',
        label: {
          vi: 'Bảng',
          en: 'Table',
        },
        path: 'component/table',
      },
      {
        code: 'componentSearch',
        label: {
          vi: 'Tìm kiếm',
          en: 'Search',
        },
        path: 'component/search',
      },
      {
        code: 'componentAside',
        label: {
          vi: 'Aside',
          en: 'Aside',
        },
        path: 'component/aside',
      },
      {
        code: 'componentTabs',
        label: {
          vi: 'Tabs',
          en: 'Tabs',
        },
        path: 'component/tabs',
      },
      {
        code: 'componentRadioCards',
        label: {
          vi: 'Radio Cards',
          en: 'Radio Cards',
        },
        path: 'component/radio-cards',
      },
    ],
  },
];
