import type { FC } from 'react';

import { LogoutOutlined, MenuFoldOutlined, MenuUnfoldOutlined, UserOutlined } from '@ant-design/icons';
import { Avatar, Dropdown, Layout, Tooltip, theme as antTheme } from 'antd';
import Cookies from 'js-cookie';
import { createElement, useEffect } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';

import EnUsSvg from '@/assets/header/en_US.svg';
import { ReactComponent as LocaleSvg } from '@/assets/header/language.svg';
import { ReactComponent as MoonSvg } from '@/assets/header/moon.svg';
import { ReactComponent as SunSvg } from '@/assets/header/sun.svg';
import { ReactComponent as ViVnSvg } from '@/assets/header/vi_VN.svg';
import AntdSvg from '@/assets/logo/antd.svg';
import { LocaleFormatter, useLocale } from '@/locales';
import { removeAllCookies } from '@/utils/functions';
import { pageRoutes } from '@/utils/pageRoutes';
import { useGlobalState } from '@/utils/useGlobalState';
import type { Locale } from '@/utils/useGlobalState';

import HeaderNoticeComponent from './notice';

const { Header } = Layout;

interface HeaderProps {
  collapsed?: boolean;
  toggle?: () => void;
}

type Action = 'userInfo' | 'userSetting' | 'logout';

const HeaderComponent: FC<HeaderProps> = ({ collapsed, toggle }) => {
  const { locale, setLocale, theme, setTheme, device } = useGlobalState();
  const params = useParams();
  const localtion = useLocation();

  console.log({ params });
  const logged = !!Cookies.get('token');
  const userInfo = JSON.parse(Cookies.get('user') as string);
  const navigate = useNavigate();
  const token = antTheme.useToken();
  const { formatMessage } = useLocale();

  useEffect(() => {
    setLocale(params.lang as Locale);
  }, []);

  const onActionClick = async (action: Action) => {
    switch (action) {
      case 'userInfo':
        return;
      case 'userSetting':
        return;
      case 'logout':
        removeAllCookies();
        navigate(pageRoutes.login);

        return;
    }
  };

  const toLogin = () => {
    navigate('/login');
  };

  const selectLocale = ({ key }: { key: string }) => {
    const lang = ['vi', 'en'].includes(key) ? key : 'en';
    const path = localtion.pathname.split('/').slice(2).join('/');

    setLocale(lang as Locale);
    navigate(`/${key}/${path}`);
  };

  const onChangeTheme = () => {
    const newTheme = theme === 'dark' ? 'light' : 'dark';

    setTheme(newTheme);
  };

  return (
    <Header className="layout-page-header bg-2" style={{ backgroundColor: token.token.colorBgContainer }}>
      {device !== 'MOBILE' && (
        <div className="logo" style={{ width: collapsed ? 80 : 200 }}>
          <img src={AntdSvg} alt="" />
        </div>
      )}
      <div className="layout-page-header-main">
        <div onClick={toggle}>
          <span id="sidebar-trigger">{collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}</span>
        </div>
        <div className="actions">
          <Tooltip
            title={formatMessage({
              id: theme === 'dark' ? 'gloabal.tips.theme.lightTooltip' : 'gloabal.tips.theme.darkTooltip',
            })}
          >
            <span>
              {createElement(theme === 'dark' ? SunSvg : MoonSvg, {
                onClick: onChangeTheme,
              })}
            </span>
          </Tooltip>
          <HeaderNoticeComponent />
          <Dropdown
            menu={{
              onClick: info => selectLocale(info),
              items: [
                {
                  key: 'vi',
                  icon: <ViVnSvg />,
                  disabled: locale === 'vi',
                  label: 'Vietnamese',
                },
                {
                  key: 'en',
                  icon: <span><EnUsSvg /></span>,
                  disabled: locale === 'en',
                  label: 'English',
                },
              ],
            }}
          >
            <span>
              <LocaleSvg id="locale-change" />
            </span>
          </Dropdown>

          {logged ? (
            <Dropdown
              menu={{
                items: [
                  {
                    key: '1',
                    icon: <UserOutlined />,
                    label: (
                      <span onClick={() => navigate('/dashboard')}>
                        <LocaleFormatter id="header.avator.account" />
                      </span>
                    ),
                  },
                  {
                    key: '2',
                    icon: <LogoutOutlined />,
                    label: (
                      <span onClick={() => onActionClick('logout')}>
                        <LocaleFormatter id="header.avator.logout" />
                      </span>
                    ),
                  },
                ],
              }}
            >
              <span className="user-action">
                <Avatar src={userInfo.avatar} />
              </span>
            </Dropdown>
          ) : (
            <span style={{ cursor: 'pointer' }} onClick={toLogin}>
              {formatMessage({ id: 'gloabal.tips.login' })}
            </span>
          )}
        </div>
      </div>
    </Header>
  );
};

export default HeaderComponent;
