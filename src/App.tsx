import 'dayjs/locale/en';
import './styles/global.less';

import { ConfigProvider, Spin, theme as a } from 'antd';
import enUS from 'antd/es/locale/en_US';
import viVn from 'antd/es/locale/vi_VN';
import dayjs from 'dayjs';
import Cookies from 'js-cookie';
import React, { Suspense, useEffect, useState } from 'react';
import { IntlProvider } from 'react-intl';
import { useNavigate } from 'react-router-dom';

import { localeConfig, LocaleFormatter } from './locales';
import LoginForm from './pages/login';
import RenderRouter from './routes';
import { pageRoutes } from './utils/pageRoutes';
import { useGlobalState } from './utils/useGlobalState';

const App: React.FC = () => {
  const { theme, locale } = useGlobalState();
  const token = Cookies.get('token');
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    dayjs.locale(locale);
  }, [locale]);
  useEffect(() => {
    if (!token) {
      navigate(`${locale}/${pageRoutes.login}`);
    }
  }, []);

  const getAntdLocale = () => {
    if (locale === 'en') {
      return enUS;
    } else if (locale === 'vi') {
      return viVn;
    }
  };

  const renderPage = () => {
    if (!token) {
      return <LoginForm />;
    }

    return (
      <>
        <RenderRouter />
      </>
    );
  };

  return (
    <ConfigProvider
      locale={getAntdLocale()}
      componentSize="middle"
      theme={{ token: { colorPrimary: '#13c2c2' }, algorithm: theme === 'dark' ? a.darkAlgorithm : a.defaultAlgorithm }}
    >
      <IntlProvider locale={locale} messages={localeConfig[locale]}>
        <Suspense fallback={<div>Loading...</div>}>
          <Spin
            spinning={loading}
            className="app-loading-wrapper"
            tip={<LocaleFormatter id="gloabal.tips.loading" />}
          ></Spin>
          {renderPage()}
        </Suspense>
      </IntlProvider>
    </ConfigProvider>
  );
};

export default App;
