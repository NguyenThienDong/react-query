import { message } from 'antd';
import axios from 'axios';
import Cookies from 'js-cookie';

import { apiRoutes } from './apiRoutes';
import { removeAllCookies } from './functions';

const axiosInstance = axios.create({
  baseURL: import.meta.env.VITE_BASE_URL,
});

// Hàm để lấy token từ local storage hoặc từ bất kỳ nguồn dữ liệu nào khác
const getToken = () => {
  return Cookies.get('token');
};

// Thêm interceptor để truyền token vào header của các yêu cầu
axiosInstance.interceptors.request.use(
  (config: any) => {
    const token = getToken();

    if (token) {
      config.headers['Authorization'] = `Bearer ${token}`;
    }

    return config;
  },
  error => {
    return Promise.reject(error);
  },
);

// Thêm interceptor để xử lý lỗi
axiosInstance.interceptors.response.use(
  response => {
    return response;
  },
  async error => {
    const originalRequest = error.config;

    console.log('originalRequest: ', originalRequest);

    if (error.response.status === 401 && !originalRequest._retry && !originalRequest.url.includes('login')) {
      originalRequest._retry = true;

      try {
        const refreshToken = Cookies.get('refresh_token');
        const msisdn = JSON.parse(Cookies.get('user') as string).phoneNumber;
        // Gọi API để làm mới token
        const resp = await axiosInstance.post(`${apiRoutes.refreshToken}`, {
          refreshToken,
          msisdn,
          requestId: '123123',
          currentTime: '123123',
        });

        if (resp?.data?.result) {
          const { access_token, refresh_token, expires_in, refresh_expires_in } = resp.data.result;

          Cookies.set('token', access_token, { expires: expires_in });
          Cookies.set('refresh_token', refresh_token, { expires: refresh_expires_in });

          return axiosInstance(originalRequest);
        }
      } catch (refreshError: any) {
        message.error(refreshError?.response?.data?.error_description);
        removeAllCookies();
        window.location.reload();

        return Promise.reject(refreshError);
      }
    }

    return Promise.reject(error);
  },
);

export default axiosInstance;
