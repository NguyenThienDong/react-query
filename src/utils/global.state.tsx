import { QueryClient } from '@tanstack/react-query';

export function getGlobalState() {
  const device = /(iPhone|iPad|iPod|iOS|Android)/i.test(navigator.userAgent) ? 'MOBILE' : 'DESKTOP';
  const collapsed = device !== 'DESKTOP';

  return {
    device,
    collapsed,
  } as const;
}

const queryClient = new QueryClient();

// Hàm lấy giá trị trạng thái toàn cục

export function getStateGlobal(key: string) {
  return queryClient.getQueryData([key]);
}

// Hàm cập nhật giá trị trạng thái toàn cục
export function setStateGlobal(key: string, value: any) {
  queryClient.setQueryData([key], value);
}
