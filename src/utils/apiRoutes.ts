export const apiRoutes = {
  // account
  login: 'account-service/public/user/family/login',
  refreshToken: 'account-service/public/user/family/refreshToken',

  // resource
  getTodoList: 'resource-service/car/getList',
  addTodo: 'resource-service/car/add',
  deleteTodo: 'resource-service/car/delete',
  exportTemplateCar: 'resource-service/car/template',
};
