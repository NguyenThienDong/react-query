import Cookies from 'js-cookie';

export function removeAllCookies() {
  const cookies = Cookies.get();

  for (const cookieName in cookies) {
    Cookies.remove(cookieName, { path: '/' });
  }
}
