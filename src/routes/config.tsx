import type { FC, ReactElement } from 'react';
import type { RouteProps } from 'react-router';

import Cookies from 'js-cookie';
import { useIntl } from 'react-intl';

import NotFoundPage from '@/pages/404';

import { history } from './history';
import PrivateRoute from './privateRoute';

export interface WrapperRouteProps extends RouteProps {
  /** document title locale id */
  titleId: string;
  /** authorization？ */
  roles?: string | string[];
}

const WrapperRouteComponent: FC<WrapperRouteProps> = ({ titleId, roles, ...props }) => {
  const { formatMessage } = useIntl();
  const lang = history.location.pathname.split('/')[1];

  console.log({roles});

  if (!['en', 'vi'].includes(lang)) {
    return <NotFoundPage />;
  }

  const token = Cookies.get('token');

  console.log({ token });

  if (titleId) {
    document.title = formatMessage({
      id: titleId,
    });
  }

  return token ? <PrivateRoute {...props} /> : (props.element as ReactElement);
};

export default WrapperRouteComponent;
